#include "Planner.hpp"
#include "math.hpp"
#include <iostream>

#define BOOST_TEST_MODULE "PlannerTest"
#include <boost/test/included/unit_test.hpp>

using namespace epp;

class Fixture
{
public:
	Fixture()
	{
		cm(0, 0, 0) = 0.1; cm(0, 1, 0) = 0.1; cm(0, 2, 0) = 0.1; cm(0, 3, 0) = 0.1; cm(0, 4, 0) = 0.1;
		cm(1, 0, 0) = 0.1; cm(1, 1, 0) = 0.1; cm(1, 2, 0) = 1.0; cm(1, 3, 0) = 1.0; cm(1, 4, 0) = 0.1;
		cm(2, 0, 0) = 0.1; cm(2, 1, 0) = 1.0; cm(2, 2, 0) = 1.0; cm(2, 3, 0) = 1.0; cm(2, 4, 0) = 0.1;
		cm(3, 0, 0) = 0.1; cm(3, 1, 0) = 1.0; cm(3, 2, 0) = 1.0; cm(3, 3, 0) = 1.0; cm(3, 4, 0) = 0.1;
		cm(4, 0, 0) = 0.1; cm(4, 1, 0) = 1.0; cm(4, 2, 0) = 1.0; cm(4, 3, 0) = 1.0; cm(4, 4, 0) = 0.1;

		cm(0, 0, 1) = 0.1; cm(0, 1, 1) = 0.1; cm(0, 2, 1) = 0.1; cm(0, 3, 1) = 0.1; cm(0, 4, 1) = 0.1;
		cm(1, 0, 1) = 0.1; cm(1, 1, 1) = 0.1; cm(1, 2, 1) = 0.1; cm(1, 3, 1) = 0.1; cm(1, 4, 1) = 0.1;
		cm(2, 0, 1) = 0.1; cm(2, 1, 1) = 0.1; cm(2, 2, 1) = 0.1; cm(2, 3, 1) = 0.1; cm(2, 4, 1) = 0.1;
		cm(3, 0, 1) = 0.1; cm(3, 1, 1) = 0.1; cm(3, 2, 1) = 0.1; cm(3, 3, 1) = 0.1; cm(3, 4, 1) = 0.1;
		cm(4, 0, 1) = 0.1; cm(4, 1, 1) = 0.1; cm(4, 2, 1) = 0.1; cm(4, 3, 1) = 0.1; cm(4, 4, 1) = 0.1;

		cm(0, 0, 2) = 0.1; cm(0, 1, 2) = 0.1; cm(0, 2, 2) = 0.1; cm(0, 3, 2) = 0.1; cm(0, 4, 2) = 0.1;
		cm(1, 0, 2) = 0.1; cm(1, 1, 2) = 0.1; cm(1, 2, 2) = 0.1; cm(1, 3, 2) = 0.1; cm(1, 4, 2) = 0.1;
		cm(2, 0, 2) = 0.1; cm(2, 1, 2) = 0.1; cm(2, 2, 2) = 0.1; cm(2, 3, 2) = 0.1; cm(2, 4, 2) = 0.1;
		cm(3, 0, 2) = 0.1; cm(3, 1, 2) = 0.1; cm(3, 2, 2) = 0.1; cm(3, 3, 2) = 0.1; cm(3, 4, 2) = 0.1;
		cm(4, 0, 2) = 0.1; cm(4, 1, 2) = 0.1; cm(4, 2, 2) = 0.1; cm(4, 3, 2) = 0.1; cm(4, 4, 2) = 0.1;

		cm(0, 0, 3) = 0.1; cm(0, 1, 3) = 0.1; cm(0, 2, 3) = 0.1; cm(0, 3, 3) = 0.1; cm(0, 4, 3) = 0.1;
		cm(1, 0, 3) = 0.1; cm(1, 1, 3) = 0.1; cm(1, 2, 3) = 0.1; cm(1, 3, 3) = 0.1; cm(1, 4, 3) = 0.1;
		cm(2, 0, 3) = 0.1; cm(2, 1, 3) = 0.1; cm(2, 2, 3) = 0.1; cm(2, 3, 3) = 0.1; cm(2, 4, 3) = 0.1;
		cm(3, 0, 3) = 0.1; cm(3, 1, 3) = 0.1; cm(3, 2, 3) = 0.1; cm(3, 3, 3) = 0.1; cm(3, 4, 3) = 0.1;
		cm(4, 0, 3) = 0.1; cm(4, 1, 3) = 0.1; cm(4, 2, 3) = 0.1; cm(4, 3, 3) = 0.1; cm(4, 4, 3) = 0.1;

		cm(0, 0, 4) = 0.1; cm(0, 1, 4) = 0.1; cm(0, 2, 4) = 0.1; cm(0, 3, 4) = 0.1; cm(0, 4, 4) = 0.1;
		cm(1, 0, 4) = 0.1; cm(1, 1, 4) = 0.1; cm(1, 2, 4) = 0.1; cm(1, 3, 4) = 0.1; cm(1, 4, 4) = 0.1;
		cm(2, 0, 4) = 0.1; cm(2, 1, 4) = 0.1; cm(2, 2, 4) = 0.1; cm(2, 3, 4) = 0.1; cm(2, 4, 4) = 0.1;
		cm(3, 0, 4) = 0.1; cm(3, 1, 4) = 0.1; cm(3, 2, 4) = 0.1; cm(3, 3, 4) = 0.1; cm(3, 4, 4) = 0.1;
		cm(4, 0, 4) = 0.1; cm(4, 1, 4) = 0.1; cm(4, 2, 4) = 0.1; cm(4, 3, 4) = 0.1; cm(4, 4, 4) = 0.1;

		planner = new Planner<5, 5, 5>(&cm);

		easyStart = create_point(0, 0, 2);
		easyEnd = create_point(0, 0, 0);
		tmp.push_back(easyEnd);
		easyPath = planner->plan(easyStart, tmp);
		tmp.clear();

		hardStart = create_point(1, 1, 0);
		hardEnd = create_point(4, 4, 0);
		tmp.push_back(hardEnd);
		hardPath = planner->plan(hardStart, tmp);
		tmp.clear();

		upStart = create_point(0, 4, 0);
		upEnd = create_point(4, 1, 4);
		tmp.push_back(upEnd);
		upPath = planner->plan(upStart, tmp);

		tmp.clear();
	}

	Matrix<float, 5, 5, 5> cm;
	Vector3i easyStart, easyEnd;
	Vector3i hardStart, hardEnd;
	Vector3i upStart, upEnd;
	Planner<5, 5, 5> *planner;
	std::vector<Vector3i> easyPath, hardPath, upPath;
	std::vector<Vector3i> tmp;
};

BOOST_AUTO_TEST_CASE(testEasyLength)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.easyPath.size(), 3);
}

BOOST_AUTO_TEST_CASE(testEasyFirst)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.easyPath[0], f.easyStart);
}

BOOST_AUTO_TEST_CASE(testEasySecond)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.easyPath[1], create_point(0, 0, 1));
}

BOOST_AUTO_TEST_CASE(testEasyEnd)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.easyPath[2], f.easyEnd);
}

BOOST_AUTO_TEST_CASE(testHardLength)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.hardPath.size(), 9);
}

BOOST_AUTO_TEST_CASE(testHardStart)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.hardPath[0], f.hardStart);
}

BOOST_AUTO_TEST_CASE(testHardIsContinuous)
{
	Fixture f;
	for (size_t i = 1; i < f.hardPath.size(); ++i)
	{
		BOOST_CHECK_EQUAL((f.hardPath[i] - f.hardPath[i - 1]).norm(),
				1);
	}
}

BOOST_AUTO_TEST_CASE(testHardEnd)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.hardPath[8], f.hardEnd);
}

BOOST_AUTO_TEST_CASE(testUpSize)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.upPath.size(), 12);
}

BOOST_AUTO_TEST_CASE(testUpStart)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.upPath[0], f.upStart);
}

BOOST_AUTO_TEST_CASE(testUpEnd)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.upPath.back(), f.upEnd);
}

BOOST_AUTO_TEST_CASE(testUpIsContinuous)
{
	Fixture f;
	for (size_t i = 1; i < f.upPath.size(); ++i)
	{
		BOOST_CHECK_EQUAL((f.upPath[i] - f.upPath[i - 1]).norm(), 1);
	}
}
