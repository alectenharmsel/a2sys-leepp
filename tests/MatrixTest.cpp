#include "Matrix.hpp"

#define BOOST_TEST_MODULE "MatrixTest"
#include <boost/test/included/unit_test.hpp>

using namespace epp;

class Fixture
{
public:
	Fixture()
	{
		a(0) = 5;
		a(1) = 5;
		a(2) = 5;
		b(0) = 12.5;
		b(1) = -43.0;
		b(2) = 430.3942;
		c(0) = 5;
		c(1) = 5;
		c(2) = 5;
		d(0) = 12.5;
		d(1) = -43.0;
		d(2) = 430.3942;
		i = mat.getTuple(20);
		j = mat.getTuple(22);
		k = mat.getTuple(104);
		l = mat.getTuple(9);
		m = mat.getTuple(31);
	}

	Matrix<int, 5, 5, 5> mat;

	Vector3i a, b;
	Vector3d c, d;

	Vector3i i, j, k, l, m;
};

BOOST_AUTO_TEST_CASE(testDistanceDouble)
{
	Fixture f;

	BOOST_CHECK_CLOSE((f.c - f.d).norm(), 428.16, 0.01);
}

BOOST_AUTO_TEST_CASE(testDistanceInt)
{
	Fixture f;

	BOOST_CHECK_CLOSE((f.a - f.b).norm(), 427.76, 0.01);
}

BOOST_AUTO_TEST_CASE(testFromPointer)
{
	int *vec_raw = new int[3];
	vec_raw[0] = 10;
	vec_raw[1] = 10;
	vec_raw[2] = 10;
	Vector3i vec(vec_raw);
	BOOST_CHECK_EQUAL(vec(0), 10);
	BOOST_CHECK_EQUAL(vec(1), 10);
	BOOST_CHECK_EQUAL(vec(2), 10);
}

BOOST_AUTO_TEST_CASE(testIY)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.i(0), 0);
}

BOOST_AUTO_TEST_CASE(testIX)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.i(1), 4);
}

BOOST_AUTO_TEST_CASE(testIZ)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.i(2), 0);
}

BOOST_AUTO_TEST_CASE(testJY)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.j(0), 2);
}

BOOST_AUTO_TEST_CASE(testJX)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.j(1), 4);
}

BOOST_AUTO_TEST_CASE(testJZ)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.j(2), 0);
}

BOOST_AUTO_TEST_CASE(testKY)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.k(0), 4);
}

BOOST_AUTO_TEST_CASE(testKX)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.k(1), 0);
}

BOOST_AUTO_TEST_CASE(testKZ)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.k(2), 4);
}

BOOST_AUTO_TEST_CASE(testLY)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.l(0), 4);
}

BOOST_AUTO_TEST_CASE(testLX)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.l(1), 1);
}

BOOST_AUTO_TEST_CASE(testLZ)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.l(2), 0);
}

BOOST_AUTO_TEST_CASE(testMY)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.m(0), 1);
}

BOOST_AUTO_TEST_CASE(testMX)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.m(1), 1);
}

BOOST_AUTO_TEST_CASE(testMZ)
{
	Fixture f;
	BOOST_CHECK_EQUAL(f.m(2), 1);
}
