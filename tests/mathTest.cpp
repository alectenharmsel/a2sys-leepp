#include "math.hpp"
#include <vector>
#include <sstream>

#define BOOST_TEST_MODULE "mathTest"
#include <boost/test/included/unit_test.hpp>

using namespace epp;

BOOST_AUTO_TEST_CASE(test_polyval_int_one)
{
	std::vector<int> arr(1);
	arr[0] = 1;
	BOOST_CHECK_EQUAL(polyval(arr, 3), 1);
}

BOOST_AUTO_TEST_CASE(test_polyval_int_two)
{
	std::vector<int> arr(2);
	arr[1] = 1;
	arr[0] = 2;
	BOOST_CHECK_EQUAL(polyval(arr, 2), 5);
}

BOOST_AUTO_TEST_CASE(test_polyval_int_three)
{
	std::vector<int> arr(3);
	arr[2] = 2;
	arr[1] = 3;
	arr[0] = -1;
	BOOST_CHECK_EQUAL(polyval(arr, 5), -8);
}

BOOST_AUTO_TEST_CASE(test_polyval_int_four)
{
	std::vector<int> arr(4);
	arr[3] = 2;
	arr[2] = 3;
	arr[1] = -1;
	arr[0] = 1;
	BOOST_CHECK_EQUAL(polyval(arr, 5), 117);
}

BOOST_AUTO_TEST_CASE(test_polyval_int_five)
{
	std::vector<int> arr(5);
	arr[4] = 2;
	arr[3] = 3;
	arr[2] = -1;
	arr[1] = 2;
	arr[0] = -1;
	BOOST_CHECK_EQUAL(polyval(arr, 5), -383);
}

BOOST_AUTO_TEST_CASE(test_polyval_double_one)
{
	std::vector<double> arr(1);
	arr[0] = 1.0;
	BOOST_CHECK_CLOSE(polyval(arr, 3.0), 1.0, 0.1);
}

BOOST_AUTO_TEST_CASE(test_polyval_double_two)
{
	std::vector<double> arr(2);
	arr[1] = 1.0;
	arr[0] = 2.0;
	BOOST_CHECK_CLOSE(polyval(arr, 2.0), 5.0, 0.1);
}

BOOST_AUTO_TEST_CASE(test_polyval_double_three)
{
	std::vector<double> arr(3);
	arr[2] = 2.0;
	arr[1] = 3.0;
	arr[0] = -1.0;
	BOOST_CHECK_CLOSE(polyval(arr, 5.0), -8.0, 0.1);
}

BOOST_AUTO_TEST_CASE(test_polyval_double_four)
{
	std::vector<double> arr(4);
	arr[3] = 2.0;
	arr[2] = 3.0;
	arr[1] = -1.0;
	arr[0] = 1.0;
	BOOST_CHECK_CLOSE(polyval(arr, 5.0), 117.0, 0.1);
}

BOOST_AUTO_TEST_CASE(test_polyval_double_five)
{
	std::vector<double> arr(5);
	arr[4] = 2.0;
	arr[3] = 3.0;
	arr[2] = -1.0;
	arr[1] = 2.0;
	arr[0] = -1.0;
	BOOST_CHECK_CLOSE(polyval(arr, 5.0), -383.0, 0.1);
}

BOOST_AUTO_TEST_CASE(test_ftc_zero)
{
	BOOST_CHECK_EQUAL(force_to_current(0), 0);
}

BOOST_AUTO_TEST_CASE(test_ftc_gets_bigger)
{
	BOOST_CHECK_GT(force_to_current(1.18), force_to_current(1.15));
}

BOOST_AUTO_TEST_CASE(test_ftc_gets_more_bigger)
{
	BOOST_CHECK_GT(force_to_current(2.18), force_to_current(2.15));
}

BOOST_AUTO_TEST_CASE(test_ftc_gets_even_more_bigger)
{
	BOOST_CHECK_GT(force_to_current(10.18), force_to_current(10.15));
}

BOOST_AUTO_TEST_CASE(test_ftc_gets_so_much_more_bigger)
{
	BOOST_CHECK_GT(force_to_current(100.18), force_to_current(100.15));
}

BOOST_AUTO_TEST_CASE(test_current_out_not_equal_to_force_in)
{
	BOOST_CHECK_NE(force_to_current(1.0), 1.0);
}

BOOST_AUTO_TEST_CASE(test_ftc_grows_exponentially)
{
	BOOST_CHECK_GT(force_to_current(10.0), 2 * force_to_current(5.0));
}

BOOST_AUTO_TEST_CASE(test_ftc_not_too_exponential)
{
	BOOST_CHECK_LT(force_to_current(10.0), 4 * force_to_current(5.0));
}

BOOST_AUTO_TEST_CASE(test_mf_straight_requires_two_forces)
{
	std::vector<Vector3i> straight;

	straight.push_back(create_point(0, 0, 0));
	straight.push_back(create_point(0, 1, 0));
	straight.push_back(create_point(0, 2, 0));

	BOOST_CHECK_EQUAL(get_accelerations(straight).size(), 3);
}

BOOST_AUTO_TEST_CASE(test_mf_one_turn_is_three_forces)
{
	std::vector<Vector3i> turn;

	turn.push_back(create_point(10, 10, 10));
	turn.push_back(create_point(10, 11, 10));
	turn.push_back(create_point(11, 11, 10));

	BOOST_CHECK_EQUAL(get_accelerations(turn).size(), 3);
}

BOOST_AUTO_TEST_CASE(test_mf_many_turns_many_forces)
{
	std::vector<Vector3i> path;

	path.push_back(create_point(10, 10, 10));
	path.push_back(create_point(11, 10, 10));
	path.push_back(create_point(11, 11, 10));
	path.push_back(create_point(11, 10, 10));
	path.push_back(create_point(11, 9, 10));
	path.push_back(create_point(11, 8, 10));
	path.push_back(create_point(11, 8, 11));
	path.push_back(create_point(11, 9, 11));
	path.push_back(create_point(12, 9, 11));

	std::vector<Vector3i> forces = get_accelerations(path);

	BOOST_CHECK_EQUAL(forces.size(), 9);

	BOOST_CHECK_EQUAL(forces[0].squaredNorm(), 1);
	BOOST_CHECK_EQUAL(forces[1].squaredNorm(), 2);
	BOOST_CHECK_EQUAL(forces[2].squaredNorm(), 4);
	BOOST_CHECK_EQUAL(forces[3].squaredNorm(), 0);
	BOOST_CHECK_EQUAL(forces[4].squaredNorm(), 0);
	BOOST_CHECK_EQUAL(forces[5].squaredNorm(), 2);
	BOOST_CHECK_EQUAL(forces[6].squaredNorm(), 2);
	BOOST_CHECK_EQUAL(forces[7].squaredNorm(), 2);
	BOOST_CHECK_EQUAL(forces[8].squaredNorm(), 1);
}

BOOST_AUTO_TEST_CASE(test_csv_to_vec)
{
	BOOST_CHECK_EQUAL(csv_to_vec("1400,800,90"),
			create_vec(1400, 800, 90));
}

BOOST_AUTO_TEST_CASE(test_csv_to_vec_again)
{
	BOOST_CHECK_EQUAL(csv_to_vec("1400,1400,96"),
			create_vec(1400, 1400, 96));
}
