#include "Battery.hpp"

#define BOOST_TEST_MODULE "BatteryTest"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(testInitialVoltage)
{
	epp::Battery bat;
	BOOST_CHECK_CLOSE(bat.voltage(),
			epp::CELL_VOLTAGE_MAX * epp::BAT_CELLS, 0.1);
}

BOOST_AUTO_TEST_CASE(testVoltageLower)
{
	epp::Battery bat;
	float start = bat.voltage();
	bat.draw(0.01, 1);
	BOOST_CHECK_LT(bat.voltage(), start);
}

BOOST_AUTO_TEST_CASE(testDrainBattery)
{
	epp::Battery bat;
	bat.draw(1, 1);
	BOOST_CHECK_CLOSE(bat.capacity(), epp::BAT_CAP - 1/3600, 0.1);
}

BOOST_AUTO_TEST_CASE(testDrainBatteryAllTheWay)
{
	epp::Battery bat;
	bat.draw(epp::BAT_CAP, 3600);
	BOOST_CHECK_SMALL(bat.capacity(), 0.1);
}

BOOST_AUTO_TEST_CASE(testDrainToJustAboveNominal)
{
	epp::Battery bat;
	bat.draw(epp::BAT_CAP, (2000 - 1483.5) / 2000 * 3600);
	BOOST_CHECK_CLOSE(bat.voltage(),
			epp::BAT_CELLS * epp::CELL_VOLTAGE_NOMINAL, .01);
}

BOOST_AUTO_TEST_CASE(testDrainBatteryQuiteABit)
{
	epp::Battery bat;
	bat.draw(epp::BAT_CAP, 3450);
	BOOST_CHECK_LT(bat.voltage(),
			epp::BAT_CELLS * epp::CELL_VOLTAGE_NOMINAL);
	BOOST_CHECK_GT(bat.voltage(), epp::BAT_CELLS * epp::CELL_VOLTAGE_MIN);
}

BOOST_AUTO_TEST_CASE(testBatteryVoltageFit)
{
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[0], .0000000000000000000008616621);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[1], -.0000000000000000068173);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[2], .0000000000000222241);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[3], -.0000000000384976);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[4], .0000000380914);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[5], -.0000213653);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[6], .0064046);
	BOOST_CHECK_EQUAL(epp::CELL_VOLTAGE_FIT[7], 2.6731);
}

BOOST_AUTO_TEST_CASE(testBatteryCostSmall)
{
	epp::Battery bat;
	double cost = bat.draw(epp::BAT_CAP, 1);
	BOOST_CHECK_CLOSE(cost, 1.0 / 3600.0, 0.1);
}

BOOST_AUTO_TEST_CASE(testBatteryCostIncreases)
{
	epp::Battery bat;
	double lower = bat.draw(epp::BAT_CAP, 1);
	double higher = bat.draw(epp::BAT_CAP, 1);
	BOOST_CHECK_LT(lower, higher);
}

BOOST_AUTO_TEST_CASE(testBatteryCostHalf)
{
	epp::Battery bat;
	double cost = bat.draw(epp::BAT_CAP, 1800);
	BOOST_CHECK_CLOSE(cost, 0.5, 0.1);
}
