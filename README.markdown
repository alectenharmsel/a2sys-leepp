# Low Energy Emergency Path Planner

This repository contains the result of research done at the University of
Michigan. The paper and the code can be built with CMake, and requires the
following libraries and tools to run:

* LaTeX (MikTeX on Windows, TeXLive on Linux)
* HDF5
* A C++ compiler (Visual Studio on Windows, GCC on Linux)
