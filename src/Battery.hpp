#ifndef EPP_BATTERY_HPP
#define EPP_BATTERY_HPP

#include "config.hpp"
#include "math.hpp"
#include <stddef.h>
#include <vector>

namespace epp
{

const double CELL_VOLTAGE_NOMINAL = 3.7;
const double CELL_VOLTAGE_MAX = 3.95;
const double CELL_VOLTAGE_MIN = 3.0;
const double CELL_VOLTAGE_FIT[] = {
	.0000000000000000000008616621,
	-.0000000000000000068173,
	.0000000000000222241,
	-.0000000000384976,
	.0000000380914,
	-.0000213653,
	.0064046,
	2.6731
};
const double CELL_VOLTAGE_FITLEN = 8;
const double CELL_VOLTAGE_FIT_SCALE = 2;

class Battery
{
public:
	Battery()
	{
		cap = BAT_CAP;
		cells = BAT_CELLS;
		capScale = CELL_VOLTAGE_FIT_SCALE / BAT_CAP;
		for (size_t i = 0; i < CELL_VOLTAGE_FITLEN; ++i)
		{
			voltFit.push_back(CELL_VOLTAGE_FIT[i]);
		}
	}

	double draw(double draw, double dt)
	{
		double beginCap = cap;
		cap -= draw * dt / 3600;
		return (1.0 - (cap / beginCap));
	}

	double draw(std::vector<double> draws, double dt)
	{
		double beginCap = cap;
		for (size_t i = 0; i < draws.size(); ++i)
		{
			cap -= draws[i] * dt / 3600;
		}
		return (1.0 - (cap / beginCap));
	}

	double voltage() const
	{
		return epp::BAT_CELLS * polyval(voltFit, capScale * mAh());
	}

	double capacity() const
	{
		return cap;
	}

	double mAh() const
	{
		return 1000.0 * cap;
	}

private:
	double cap;
	double cells;
	double capScale;
	std::vector<double> voltFit;
};

} //namespace epp

#endif //EPP_BATTERY_HPP
