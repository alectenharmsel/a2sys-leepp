#include "Matrix.hpp"
#include "Planner.hpp"
#include "config.hpp"
#include "math.hpp"
#include <getopt.h>
#include <hdf5.h>
#include <iostream>
#include <string.h>

int run(const std::string &fname_cm, const std::string &fname_goals,
		const epp::Vector3i &start_pos);
int run(const std::string &fname_cm, const std::string &fname_goals,
		const std::string &fname_starts);
int run(const std::string &fname_cm, const std::string &fname_goals,
		const std::vector<epp::Vector3i> &starts);

void sanity_check()
{
	using namespace std;
	using namespace epp;
	cout << "Motor current (A) at 10N of thrust: " << force_to_current(10);
	cout << endl;
}

size_t get_hdf5_size(const std::string &fname, const char *dbname)
{
	hid_t f = H5Fopen(fname.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	assert(f > 0);
	hid_t g = H5Gopen(f, dbname, H5P_DEFAULT);
	assert(g > 0);
	hid_t d = H5Dopen(g, "value", H5P_DEFAULT);
	assert(d > 0);
	assert(H5Dget_type(d) > 0);
	size_t num = H5Sget_simple_extent_npoints(H5Dget_space(d));
	assert(num > 0);

	return num;
}

float * read_hdf5(const std::string &fname, const char *dbname)
{
	double *ret = 0;

	hid_t f = H5Fopen(fname.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	assert(f > 0);
	hid_t g = H5Gopen(f, dbname, H5P_DEFAULT);
	assert(g > 0);
	hid_t d = H5Dopen(g, "value", H5P_DEFAULT);
	assert(d > 0);
	assert(H5Dget_type(d) > 0);
	size_t num = H5Sget_simple_extent_npoints(H5Dget_space(d));
	assert(num > 0);

#ifndef NDEBUG
	std::cout << fname << " contains " << num << " data points" << "\n";
	std::cout << std::flush;
#endif //NDEBUG

	ret = new double[num];

	for (size_t i = 0; i < num; ++i)
	{
		ret[i] = 0;
	}

	assert(ret != 0);
	assert(ret != NULL);

	if (H5Dread(d, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ret) < 0)
	{
		delete[] ret;
		return 0;
	}

	H5Dclose(d);
	H5Fclose(f);

	float * real_ret = new float[num];

	for (size_t i = 0; i < num; ++i)
	{
		real_ret[i] = (float) ret[i];
	}

	delete[] ret;

	return real_ret;
}

std::vector<epp::Vector3i> goals_from_col_major_floats(float *dubs,
		size_t num_dubs)
{
	using namespace epp;

	assert((num_dubs % 3) == 0);

	std::vector<Vector3i> ret;
	size_t first = 0;
	size_t second = num_dubs / 3;

	for (size_t third = 2 * second; third < num_dubs; ++third)
	{
		ret.push_back(create_vec(static_cast<int>(dubs[first++]),
					static_cast<int>(dubs[second++]),
					static_cast<int>(dubs[third])));
	}

	return ret;
}

int run(const std::string &fname_cm, const std::string &fname_goals,
		const epp::Vector3i &start_pos)
{
	std::vector<epp::Vector3i> vec;
	vec.push_back(start_pos);

	return run(fname_cm, fname_goals, vec);
}

int run(const std::string &fname_cm, const std::string &fname_goals,
		const std::string &fname_starts)
{
	float *starts_raw = read_hdf5(fname_starts, "starts");
	std::vector<epp::Vector3i> starts = goals_from_col_major_floats(
			starts_raw, get_hdf5_size(fname_starts, "starts"));

	delete[] starts_raw;

	return run(fname_cm, fname_goals, starts);
}

int run(const std::string &fname_cm, const std::string &fname_goals,
		const std::vector<epp::Vector3i> &starts)
{
	using namespace epp;
	Matrix<float, CM_YDIM, CM_XDIM, CM_ZDIM> cm(read_hdf5(fname_cm,
				"cm"));
	float *goals_raw = read_hdf5(fname_goals, "goals");
	std::vector<Vector3i> goals = goals_from_col_major_floats(
			goals_raw, get_hdf5_size(fname_goals, "goals"));

	delete[] goals_raw;

	std::vector<Vector3i> path;

	path.reserve(200);

#ifndef NDEBUG
	std::cout << "Done loading map" << std::endl;
	std::cout << "Starting to plan path..." << std::endl;
#endif //NDEBUG

	Planner<CM_YDIM, CM_XDIM, CM_ZDIM> planner(&cm);

	for (size_t i = 0; i < starts.size(); ++i)
	{
		path = planner.plan(starts[i], goals);
		for (size_t i = 0; i < path.size(); ++i)
		{
			std::cout << path[i] << std::endl;
		}
		std::cout << std::endl;
	}

	return 0;
}

int main(int argc, char **argv)
{
	using namespace epp;

	int do_sanity_check = 0;
	struct option opts[] = {
		{"sanity-check", no_argument, &do_sanity_check, 1}
	};
	int getopt_i = 0;

	while (getopt_long(argc, argv, "", opts, &getopt_i) >= 0)
	{
	}

	if (do_sanity_check)
	{
		sanity_check();
		return 0;
	}

	if (H5open() < 0)
	{
		std::cout << "HDF5 library error" << std::endl;
		return 1;
	}

	bool multistart = false;
	std::string fname_cm, fname_goals, start;

	for (int i = argc - 1; i >= 0; --i)
	{
		if (strstr(argv[i], "_cm.h5"))
		{
			fname_cm = std::string(argv[i]);
		}
		else if (strstr(argv[i], "_goals.h5"))
		{
			fname_goals = std::string(argv[i]);
		}
		else if (strstr(argv[i], "_starts.h5"))
		{
			multistart = true;
			start = std::string(argv[i]);
		}
		else if (strstr(argv[i], ","))
		{
			start = std::string(argv[i]);
		}
	}

	if (fname_cm.size() == 0 ||
			fname_goals.size() == 0 ||
			start.size() == 0)
	{
		std::cerr << "Oops, you forgot a parameter" << std::endl;
		return 1;
	}

#ifndef NDEBUG
	std::cout << "Opening cost map " << fname_cm << "\n";
	std::cout << "Opening goal location array " << fname_goals << "\n";
	std::cout << "Starting at " << start << std::endl;
#endif //NDEBUG

	int ret = 0;

	if (!multistart)
	{
		Vector3i pos = csv_to_vec(start);
		ret = run(fname_cm, fname_goals, pos);
	}
	else
	{
		ret = run(fname_cm, fname_goals, start);
	}

	return ret;
}
