#ifndef EPP_MATRIX_HPP
#define EPP_MATRIX_HPP

#include <cstddef>
#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>

namespace epp
{

template <typename T, size_t ydim, size_t xdim, size_t zdim>
class Matrix
{
public:
	Matrix()
	{
		_data = new T[(ydim * xdim * zdim)];
		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
			_data[i] = 0;
	}

	Matrix(const Matrix<T, ydim, xdim, zdim> &rhs)
	{
		_data = new T[(ydim * xdim * zdim)];
		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
			_data[i] = rhs._data[i];
	}

	Matrix(T *data)
	{
		_data = data;
	}

	~Matrix()
	{
		delete[] _data;
	}

	void operator =(const Matrix<T, ydim, xdim, zdim> &rhs)
	{
		_data = new T[(ydim * xdim * zdim)];
		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
			_data[i] = rhs._data[i];
	}

	T & operator()(size_t y, size_t x = 0, size_t z = 0)
	{
		return _data[getIndex(y, x, z)];
	}

	const T & operator()(size_t y, size_t x = 0, size_t z = 0) const
	{
		return _data[getIndex(y, x, z)];
	}

	bool operator==(const Matrix<T, ydim, xdim, zdim> &rhs) const
	{
		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
		{
			if (rhs._data[i] != _data[i])
				return false;
		}
		return true;
	}

	bool operator!=(const Matrix<T, ydim, xdim, zdim> &rhs) const
	{
		return !(*this == rhs);
	}

	T squaredNorm() const
	{
		assert(xdim == 1);
		assert(zdim == 1);

		T ret = 0;
		T tmp = 0;

		for (size_t i = 0; i < ydim; ++i)
		{
			tmp = operator()(i, 0, 0);
			ret += tmp * tmp;
		}

		return ret;
	}

	double norm() const
	{
		return sqrt(squaredNorm());

		return 0;
	}

	Matrix<T, ydim, xdim, zdim> operator+(
			const Matrix<T, ydim, xdim, zdim> &rhs) const
	{
		Matrix<T, ydim, xdim, zdim> ret;

		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
		{
			ret._data[i] = _data[i] +
				rhs._data[i];
		}

		return ret;
	}

	Matrix<T, ydim, xdim, zdim> operator-(
			const Matrix<T, ydim, xdim, zdim> &rhs) const
	{
		Matrix<T, ydim, xdim, zdim> ret;

		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
		{
			ret._data[i] = _data[i] -
				rhs._data[i];
		}

		return ret;
	}

	Matrix<T, ydim, xdim, zdim> operator*(T scalar) const
	{
		Matrix<T, ydim, xdim, zdim> ret;

		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
		{
			ret._data[i] = _data[i] * scalar;
		}

		return ret;
	}

	std::string str() const
	{
		std::stringstream s;

		for (size_t i = 0; i < (ydim * xdim * zdim); ++i)
		{
			s << _data[i] << " ";
		}

		return s.str();
	}

	operator const char* () const
	{
		return str().c_str();
	}

	size_t getIndex(size_t y, size_t x = 1, size_t z = 1) const
	{
		size_t ret = y;

		ret += (ydim * x);
		ret += (ydim * xdim * z);

		assert(ret < (ydim * xdim * zdim));

		return ret;
	}

	Matrix<int, 3, 1, 1> getTuple(size_t i) const
	{
		assert(i < (ydim * xdim * zdim));

		Matrix<int, 3, 1, 1> ret;
		ret(0) = i % ydim;
		ret(2) = i / (ydim * xdim);
		i -= ret(2) * ydim * xdim;
		ret(1) = i / xdim;

		return ret;
	}

private:
	T *_data;
};

typedef Matrix<int, 3, 1, 1> Vector3i;
typedef Matrix<double, 3, 1, 1> Vector3d;

} //namespace epp

std::ostream & operator<<(std::ostream &s, const epp::Vector3i &pt)
{
	return s << "(" << pt(0) << "," << pt(1) << "," << pt(2) << ")";
}

#endif //EPP_MATRIX_HPP
