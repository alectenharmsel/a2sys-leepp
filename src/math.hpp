#ifndef EPP_MATH_HPP
#define EPP_MATH_HPP

#include <vector>
#include <cmath>
#include <cstddef>
#include "Matrix.hpp"
#include <cstdlib>

namespace epp
{

const double CELL_AMP_FIT[] = {
	.0299389,
	1.8000,
	0.0000
};
const double CELL_AMP_FITLEN = 3;
static std::vector<double> amp_fit;

template <typename T>
inline T polyval(const std::vector<T> &poly, const T x)
{
	T ret = 0;

	for (int i = poly.size() - 1; i >= 0; --i)
	{
		ret += poly[i] * pow(x, poly.size() - 1 - i);
	}

	return ret;
}

inline double force_to_current(double f)
{
	if (amp_fit.empty())
	{
		for (size_t i = 0; i < CELL_AMP_FITLEN; ++i)
		{
			amp_fit.push_back(CELL_AMP_FIT[i]);
		}
	}
	return polyval(amp_fit, f);
}

inline std::vector<double> force_to_current(std::vector<double> f)
{
	std::vector<double> ret;
	for (size_t i = 0; i < f.size(); ++i)
	{
		ret.push_back(force_to_current(f[i]));
	}
	return ret;
}

template <typename T>
inline Matrix<T, 3, 1, 1> create_point(T x, T y, T z)
{
	Matrix<T, 3, 1, 1> ret;
	ret(0) = x;
	ret(1) = y;
	ret(2) = z;

	return ret;
}

template <typename T>
inline Matrix<T, 3, 1, 1> create_vec(T x, T y, T z)
{
	return create_point(x, y, z);
}

inline std::vector<epp::Vector3i> get_accelerations(
		const std::vector<Vector3i> &path)
{
	std::vector<epp::Vector3i> ret;
	std::vector<epp::Vector3i> moves;
	epp::Vector3i last;
	epp::Vector3i dir;

	assert(path.size() > 1);
	moves.push_back(create_point(0, 0, 0));

	for (size_t i = 1; i < path.size(); ++i)
	{
		moves.push_back(path[i] - path[i - 1]);
	}

	moves.push_back(create_point(0, 0, 0));

	for (size_t i = 1; i < moves.size(); ++i)
	{
		epp::Vector3i tmp = moves[i] - moves[i - 1];
		ret.push_back(tmp);
	}

	return ret;
}

void add_gravity(Vector3d &vec, double mass)
{
	vec(2) += mass * 9.8;
}

void add_gravity(std::vector<Vector3d> &vec, double mass)
{
	for (size_t i = 0; i < vec.size(); ++i)
	{
		add_gravity(vec[i], mass);
	}
}

Vector3d i2d(const Vector3i &vec)
{
	return create_vec(static_cast<double>(vec(0)),
				static_cast<double>(vec(1)),
				static_cast<double>(vec(2)));
}

std::vector<Vector3d> i2d(const std::vector<Vector3i> &vec)
{
	std::vector<Vector3d> ret;
	for (size_t i = 0; i < vec.size(); ++i)
	{
		ret.push_back(i2d(vec[i]));
	}
	return ret;
}

template <typename T>
inline T max(const T &lhs, const T &rhs)
{
	if (lhs > rhs)
		return lhs;
	return rhs;
}

inline Vector3i csv_to_vec(const std::string &str)
{
	Vector3i blah;
	size_t i = str.find(',') + 1;
	blah(0) = atoi(str.substr(0, i).c_str());
	blah(1) = atoi(str.substr(i, str.find(',', i) + 1).c_str());
	i = str.find(',', i) + 1;
	blah(2) = atoi(str.substr(i).c_str());
	return blah;
}

} //namespace epp

#endif //EPP_MATH_HPP
