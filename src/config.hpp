#ifndef EPP_CONFIG_H
#define EPP_CONFIG_H

namespace epp
{

const static unsigned int BAT_CELLS = 4;
const static double BAT_CAP = 4.0;
const static double UAS_MASS = 1.5;
const static double UAS_SPEED = 2.0;

} //namespace epp

#define CM_YDIM 1501
#define CM_XDIM 1501
#define CM_ZDIM 96
#define CM_MAX_GOAL_COST 0.1

#endif //EPP_CONFIG_H
