#ifndef EPP_PLANNER_HPP
#define EPP_PLANNER_HPP

#include "Matrix.hpp"
#include "Battery.hpp"
#include "math.hpp"
#include "config.hpp"
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <stdint.h>

namespace epp
{

template<size_t y, size_t x, size_t z>
class Planner
{
public:
	Planner(Matrix<float, y, x, z> *costMap) :
		cm(costMap), running(false)
	{
		dirs.push_back(create_point(-1, 0, 0));
		dirs.push_back(create_point(1, 0, 0));
		dirs.push_back(create_point(0, -1, 0));
		dirs.push_back(create_point(0, 1, 0));
		dirs.push_back(create_point(0, 0, -1));
		dirs.push_back(create_point(0, 0, 1));
		Battery bat;
		Vector3d g;
		add_gravity(g, UAS_MASS);
		moveCostMin = bat.draw(force_to_current(g.norm()), 1);
	}

	void expand(uint32_t cell, const Vector3i &previousVelocity,
			const std::vector<Vector3i> &end)
	{
		assert(cell < (y * x * z));

		Vector3i cellPos = cm->getTuple(cell);
		float cellCost = costs[cell];
		costs[cell] = x * y * z;

		if (visited[cell])
		{
			return;
		}

#ifndef NDEBUG
		std::cout << "Expanding " << cellPos << std::endl;
#endif //NDEBUG

		float cost = 0.0;

		for (size_t j = 0; j < dirs.size(); ++j)
		{
			Vector3i pos = cellPos + dirs[j];

			if (pos(0) < 0 || pos(0) >= (int)y || pos(1) < 0 ||
					pos(1) >= (int)x || pos(2) < 0 ||
					pos(2) >= (int)z)
			{
				continue;
			}

			uint32_t i = cm->getIndex(pos(0), pos(1), pos(2));

			if (visited[i])
			{
				continue;
			}

			Battery bat;

			Vector3i accI = (pos - cellPos) - previousVelocity;

			Vector3d acc = i2d(accI) * (UAS_MASS * UAS_SPEED);
			add_gravity(acc, UAS_MASS);

			cost = cm->operator()(pos(0), pos(1), pos(2));
			if (cost >= 1.0)
			{
				continue;
			}
			cost += cellCost;
			cost += bat.draw(force_to_current(acc.norm()), 1);

			if ((cost + heuristic(i, end)) <
					(costs[curLowest] +
					 heuristic(curLowest, end)))
			{
				curLowest = i;
			}

			for (size_t j = 0; j < end.size(); ++j)
			{
				if (pos == end[j])
				{
					chosenGoal = pos;
					running = false;
				}
			}

			costs[i] = cost;
			parents[i] = cell;
		}
		costs[cell] = x * y * z;
		visited[cell] = true;
	}

	std::vector<Vector3i> plan(const Vector3i &start,
			std::vector<Vector3i> &end)
	{
		std::vector<Vector3i> path;
		Vector3i tmpVec, parVec;
		running = true;

		parents.resize(x * y * z, (x * y * z) + 1);
		costs.resize(x * y * z, (x * y * z));
		visited.resize(x * y * z, false);

		uint32_t tmp = cm->getIndex(start(0), start(1), start(2));
		costs[tmp] = cm->operator()(start(0), start(1), start(2));
		curLowest = tmp;

#ifndef NDEBUG
		std::cout << "[PathPlanner] Done with initialization" <<
			std::endl;
#endif //NDEBUG

		while (running)
		{
			parVec = cm->getTuple(tmp);
			tmp = curLowest;
			tmpVec = cm->getTuple(tmp);

			if (running)
				expand(tmp, tmpVec - parVec, end);
		}

		path.push_back(chosenGoal);

#ifndef NDEBUG
		std::cout << "[PathPlanner] Done finding path" << std::endl;
#endif //NDEBUG

		while (parents[tmp] != (y * z * x) + 1)
		{
			path.push_back(cm->getTuple(tmp));
			tmp = parents[tmp];
		}

		path.push_back(cm->getTuple(tmp));
		std::reverse(path.begin(), path.end());
		parents.clear();
		costs.clear();
		visited.clear();

		return path;
	}

	float heuristic(uint32_t i, const std::vector<Vector3i> &end) const
	{
		assert(i < (y * x * z));

		Vector3i pt = cm->getTuple(i);
		float ret = (float) (pt - end.at(0)).norm();
		float tmp = 0.0;
		for (size_t i = 1; i < end.size(); ++i)
		{
			tmp = (float) (pt - end.at(i)).norm();
			if (tmp < ret)
				ret = tmp;
		}
		//return ret * (moveCostMin + CM_MAX_GOAL_COST);
		return ret * CM_MAX_GOAL_COST;
	}

private:
	Matrix<float, y, x, z> *cm;
	std::vector<uint32_t> parents;
	std::vector<float> costs;
	std::vector<bool> visited;
	std::vector<Vector3i> dirs;
	Vector3i chosenGoal;
	float moveCostMin;
	uint32_t curLowest;
	bool running;
};

} //namespace epp

#endif //EPP_PLANNER_HPP
